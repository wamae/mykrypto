import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.toptoche.searchablespinnerlibrary.SearchableSpinner
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ke.co.svs.mykrypto.Crypto
import ke.co.svs.mykrypto.CryptoAPIService
import ke.co.svs.mykrypto.CryptoAdapter
import ke.co.svs.mykrypto.CryptoDetail
import ke.co.svs.mykrypto.*
import ke.co.svs.mykrypto.utils.CurrencyUtils
import ke.co.svs.mykrypto.utils.PrefUtil
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.json.JSONArray
import org.json.JSONObject


class HomeFragment : Fragment() {

    val cryptoAPIService by lazy {
        CryptoAPIService.create()
    }

    var disposable: Disposable? = null

    var cryptoDetails: MutableList <CryptoDetail>? = null
    var mRecyclerView: RecyclerView? = null

    var allCryptos: MutableList <Crypto>? = null
    var addCrypto: FloatingActionButton? = null
    var selectedCrypto: Crypto? = null
    var mProgressDialog:ProgressDialog? = null

    companion object {
        private var mContext: Context? = null

        val TAG: String = HomeFragment::class.java.simpleName
        fun newInstance() = HomeFragment()

        var myTotalValue: Double? = 0.0
        var mToolbar: CollapsingToolbarLayout? = null

        var currencyName: String = ""
        var myCryptos: MutableList <Crypto>? = null
        var myCryptosList = ArrayList<Crypto>()

        var mainCryptoAdapter: CryptoAdapter? = null

        fun removeCrypto(id: String){
            Log.i(TAG,"removeCrypto: ${id}")

            for (i in myCryptosList.indices) {
                if(myCryptosList.get(i).id.equals(id)){
                    myCryptosList.removeAt(i)
                    break;
                }
            }

            for (i in myCryptos?.indices!!) {
                if(myCryptos!!.get(i).id.equals(id)){
                    myCryptos!!.removeAt(i)
                    break;
                }
            }

            mainCryptoAdapter?.notifyDataSetChanged()

            PrefUtil.setMyCrypto(mContext,Gson().toJson(myCryptosList))

            myTotalValue =  calculateTotalValue(myCryptosList)
            val convertedTotalValue: Double? = CurrencyUtils.convertToUserCurrency(mContext,myTotalValue)
            mToolbar!!.title = "${currencyName} %,.2f".format(convertedTotalValue)

        }

        private fun calculateTotalValue(myCryptos: MutableList<Crypto>?): Double? {
            var totalValue =0.00
            if (myCryptos != null) {
                for(crypto in myCryptos){
                    Log.i(TAG,"calculateTotalValue: ${crypto.priceUSD} * ${crypto.myQuantity}")
                    totalValue += (crypto.priceUSD?.let { crypto.myQuantity?.times(it) }!!)
                }
            }
            return totalValue
        }

        fun editCryptoQuantity(id: String, newQuantity: Double){
            Log.i(TAG,"editCryptoQuantity: $id ")

            for (i in myCryptosList.indices) {
                if(myCryptosList.get(i).id.equals(id)){
                    myCryptosList.get(i).myQuantity = newQuantity
                    break;
                }
            }

            for (i in myCryptos?.indices!!) {
                if(myCryptos!!.get(i).id.equals(id)){
                    myCryptos!!.get(i).myQuantity = newQuantity
                    break;
                }
            }

            mainCryptoAdapter?.notifyDataSetChanged()
            PrefUtil.setMyCrypto(mContext,Gson().toJson(myCryptosList))

            myTotalValue =  calculateTotalValue(myCryptosList)
            val convertedTotalValue: Double? = CurrencyUtils.convertToUserCurrency(mContext,myTotalValue)
            mToolbar!!.title = "${currencyName} %,.2f".format(convertedTotalValue)
        }

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.home_fragment, container, false)

        mToolbar = view?.findViewById(R.id.toolbar_layout)
        mToolbar!!.title = "Loading..."

        /*val toolbar: Toolbar? = view?.findViewById(R.id.custom_toolbar)

        if (toolbar != null) {
            //(activity as AppCompatActivity).setSupportActionBar(toolbar)
            toolbar?.title = "Networth"
        }*/

        mProgressDialog = indeterminateProgressDialog("Please wait a bit…", "Fetching data")
        mProgressDialog?.setCanceledOnTouchOutside(false)

        mRecyclerView = view?.findViewById(R.id.recycler_view)
        mRecyclerView!!.layoutManager = LinearLayoutManager(activity.applicationContext, LinearLayout.VERTICAL, false)

        addCrypto = view?.findViewById(R.id.fab)
        addCrypto?.setOnClickListener { view ->
            showAddDialog();
        }

        addCrypto?.isEnabled = false
        setHasOptionsMenu(true)

        return view
    }

    private fun showAddDialog() {
        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_dialog, null)
        dialogBuilder.setView(dialogView)

        var adapter: ArrayAdapter<Crypto> = ArrayAdapter(activity,
                android.R.layout.simple_dropdown_item_1line, allCryptos)

        var autoCompleteCrypto: SearchableSpinner = dialogView.findViewById(R.id.autocomplete)
        autoCompleteCrypto.setAdapter(adapter)

        var etQuantity: EditText = dialogView.findViewById(R.id.crypto_quantity)

        autoCompleteCrypto.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                selectedCrypto = allCryptos!!.get(position)
                Log.i(TAG, "onItemSelectedListener: " + selectedCrypto!!.symbol)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }

        dialogBuilder.setTitle("Add Crypto")
        dialogBuilder.setPositiveButton("Add", { dialog, whichButton ->

            var quantity = etQuantity.text.toString().trim()
            if (quantity.isEmpty()) {
                Toast.makeText(activity, "Fill the quantity", Toast.LENGTH_SHORT).show()
            } else {
                if (myCryptosList.contains(selectedCrypto)) {
                    Toast.makeText(activity, "${selectedCrypto?.name} was already added", Toast.LENGTH_SHORT).show()
                } else {
                    Log.i(TAG, "quantity: " + quantity.toDouble())
                    myCryptosList.add(Crypto(
                            selectedCrypto?.id,
                            quantity.toDouble(),
                            null,
                            selectedCrypto?.name,
                            selectedCrypto?.symbol,
                            selectedCrypto?.rank,
                            selectedCrypto?.priceUSD,
                            selectedCrypto?.priceBTC,
                            selectedCrypto?.volumeUSD_24h,
                            selectedCrypto?.marketCapUSD,
                            selectedCrypto?.availableSupply,
                            selectedCrypto?.totalSupply,
                            selectedCrypto?.maxSupply,
                            selectedCrypto?.percentChange_1h,
                            selectedCrypto?.percentChange_24h,
                            selectedCrypto?.percentChange_7d,
                            selectedCrypto?.lastUpdated))

                    val gson = Gson()

                    PrefUtil.setMyCrypto(mContext,gson.toJson(myCryptosList))
                    getCryptoDetails()

                }
            }

        })
        dialogBuilder.setNegativeButton("Cancel", { dialog, whichButton ->

        })
        val b = dialogBuilder.create()
        b.show()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initListeners()
        getCryptoDetails()
    }


    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_settings ->
                return true
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun initListeners() {
        swipe_container.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {

            getCryptoDetails()
        })
    }

    private fun initData() {
        mContext = activity.applicationContext

        currencyName = PrefUtil.getCurrencyName(context)

        val gson: Gson = GsonBuilder().create();
        Log.i(TAG, "My crypto prefs: ${PrefUtil.getMyCryptos(mContext)}")
        val mySavedCrypto = PrefUtil.getMyCryptos(mContext)

        if(!mySavedCrypto.equals("")){
            myCryptosList = gson.fromJson(PrefUtil.getMyCryptos(mContext), object : TypeToken<List<Crypto>>() {}.type)
        }

    }

    private fun getCryptos() {
        disposable =
                cryptoAPIService.getMyCryptos(25)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result -> showResult(result) },
                                { error -> showError(error.message) }
                        )
    }

    private fun getCryptoDetails() {
        mProgressDialog?.show()
        disposable =
                cryptoAPIService.getCryptoDetails("https://min-api.cryptocompare.com/data/all/coinlist")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result -> showCryptoDetails(result) },
                                { error -> showCryptoDetailsError(error.message) }
                        )
    }

    private fun showCryptoDetailsError(message: String?) {
        Log.i(TAG, "getCryptoDetails Error: " + message)
    }

    private fun showCryptoDetails(crypto: JsonObject?) {
        val gson = GsonBuilder().create()
        var data: JSONObject? = JSONObject(gson.toJson(crypto?.getAsJsonObject("Data")));
        val x = data?.keys();
        val jsonArray = JSONArray()

        while (x?.hasNext()!!) {
            val key = x.next() as String
            jsonArray.put(data?.get(key))
        }

        Log.i(TAG, "showCryptoDetails: " + jsonArray.toString())
        cryptoDetails = gson.fromJson(jsonArray.toString(), object : TypeToken<List<CryptoDetail>>() {}.type)

        getCryptos()
    }

    private fun showError(message: String?) {
        Log.i(TAG, "getCryptos Error: " + message)
        mProgressDialog?.dismiss()
        swipe_container.setRefreshing(false);
    }

    private fun showResult(allCryptos: MutableList <Crypto>) {
        Log.i(TAG, "getCryptos: " + allCryptos.toString())
        this.allCryptos = allCryptos

        addCrypto?.isEnabled = true

        myCryptos = this.filterMyCryptos(allCryptos, myCryptosList)
        myCryptos = this.addIcons(myCryptos, cryptoDetails);

        mainCryptoAdapter = CryptoAdapter(myCryptos, mContext,currencyName){
            //showPopupMenu(view);
        }

        mRecyclerView?.adapter = mainCryptoAdapter
        mainCryptoAdapter?.notifyDataSetChanged()

        mProgressDialog?.dismiss()

        swipe_container.setRefreshing(false)
    }

    /**
     * Add Icons
     */
    private fun addIcons(cryptos: MutableList <Crypto>?, cryptoDetails: MutableList <CryptoDetail>?): MutableList <Crypto>? {

        if (cryptos != null) {
            for (crypto in cryptos) {
                if (cryptoDetails != null) {
                    for (cryptoDetail in cryptoDetails) {
                        if (crypto.symbol.equals(cryptoDetail.symbol)) {
                            crypto.imageUrl = "https://www.cryptocompare.com" + cryptoDetail.imageUrl
                            Log.i(TAG, "addIcons symbol: " + crypto.symbol + " details: " + cryptoDetail.symbol)
                            Log.i(TAG,"image: https://www.cryptocompare.com"+cryptoDetail.imageUrl)
                        } else if (crypto.symbol.equals("MIOTA") && cryptoDetail.symbol.equals("IOT")) {
                            crypto.imageUrl = "https://www.cryptocompare.com" + cryptoDetail.imageUrl
                        }
                    }
                }
            }
        }
        return cryptos;
    }

    /**
     * Filter against a users preferences
     */
    private fun filterMyCryptos(allCryptos: List<Crypto>, myCryptos: ArrayList<Crypto>): MutableList <Crypto>? {
        myTotalValue = 0.0
        var tempCryptoList = ArrayList<Crypto>()
        for (crypto in allCryptos) {
            var id = crypto.id
            for (myCrypto in myCryptos) {
                var myId = myCrypto.id
                if (id == myId) {
                    Log.i(TAG, "My crypto id: " + myCrypto.id + " ALL id: " + crypto.id)
                    val myQuantity: Double? = myCrypto.myQuantity
                    Log.i(TAG, "myQuantity: " + myCrypto.myQuantity)
                    crypto.myQuantity = myQuantity

                    val myValue: Double? = crypto.priceUSD!! * myQuantity!!

                    myTotalValue = myTotalValue?.plus(myValue!!)
                    Log.i(TAG, "myTotalValue: %,.2f".format(myTotalValue))

                    tempCryptoList.add(crypto)
                }
            }
        }
        Log.i(TAG, "currencyPref: " + "(" + currencyName + ") %,.2f".format(myTotalValue))
        val convertedTotalValue: Double? = CurrencyUtils.convertToUserCurrency(mContext,myTotalValue)
        mToolbar!!.title = "${currencyName} %,.2f".format(convertedTotalValue)

        return tempCryptoList
    }

}
