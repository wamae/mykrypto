package ke.co.svs.mykrypto

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*
import org.jetbrains.anko.toast


class SignupActivity : AppCompatActivity() {

    val TAG: String = SignupActivity::class.java.simpleName
    private var auth: FirebaseAuth? = null
    private var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        auth = FirebaseAuth.getInstance()

        mContext = applicationContext

        btnResetPassword.setOnClickListener({
            startActivity(Intent(this@SignupActivity, ResetPasswordActivity::class.java))
        })

        btnSignIn.setOnClickListener({
            startActivity(Intent(this@SignupActivity, LoginActivity::class.java))
        })

        btnSignUp.setOnClickListener({
            val email = etEmail?.getText().toString().trim()
            val password = etPassword?.getText().toString().trim()

            if (TextUtils.isEmpty(email)) {
                Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password.length < 6) {
                Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            progressBar.visibility = View.VISIBLE

            auth?.createUserWithEmailAndPassword(email, password)?.addOnCompleteListener { task ->
                progressBar.visibility = View.GONE
                if (task.isSuccessful) {
                    //Registration OK
                    val firebaseUser = this?.auth?.currentUser!!
                    Log.i(TAG,"registration complete")
                    startActivity(Intent(this@SignupActivity, LoginActivity::class.java))
                } else {
                    //Registration error
                    Log.i(TAG,"registration failed: ")
                    toast("Registration failed!")
                }
            }

        })

    }
}
