package ke.co.svs.mykrypto

import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.squareup.picasso.Picasso
import com.toptoche.searchablespinnerlibrary.SearchableSpinner
import ke.co.svs.mykrypto.utils.CurrencyUtils
import ke.co.svs.mykrypto.utils.PrefUtil
import org.jetbrains.anko.layoutInflater

/**
 * Created by Wamae on 25-Dec-17.
 */
class CryptoAdapter(val cryptoList: List<Crypto>?, val context: Context?, val currencyPref: String, val listener: (Int) -> Unit) : RecyclerView.Adapter<CryptoAdapter.ViewHolder>() {

    companion object {
        var selectedCrypto: Crypto? = null
        val TAG: String = CryptoAdapter::class.java.simpleName
    }


    var currencyName: String? = PrefUtil.getCurrencyName(context);
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.tvName?.text = cryptoList?.get(position)?.name
        holder?.tvSymbol?.text = cryptoList?.get(position)?.symbol

        var price: Double? = CurrencyUtils.convertToUserCurrency(context,cryptoList?.get(position)?.priceUSD)
        holder?.tvPrice?.text = "${currencyName} %,.2f".format(price)

        var percentChang1h = cryptoList?.get(position)?.percentChange_1h

        var valueChange1h: Double? = CurrencyUtils.convertToUserCurrency(context,(percentChang1h!! / 100) * price!!)

        var valueChange1hFormatted: String = "(${currencyName} %,.2f)".format(valueChange1h)

        Log.i(TAG, valueChange1hFormatted);

        holder?.tvPercent1h?.text = percentChang1h.toString() + "% " + valueChange1hFormatted

        var percentColor: Int? = getPercentColor(percentChang1h);
        holder?.tvPercent1h?.setTextColor(percentColor!!);

        var myQuantity: Double? = cryptoList?.get(position)?.myQuantity!!
        holder?.tvMyQuantity?.text = myQuantity.toString()

        var myValue: Double? = price * myQuantity!!

        holder?.tvMyValue?.text = "(${currencyName} %,.2f)".format(myValue)

        Log.i("CryptoAdapter: ", "" + cryptoList[position].imageUrl)

        Picasso.with(context)
                .load(cryptoList[position].imageUrl)
                .resize(50, 50)
                .centerCrop().into(holder?.ivICoin)
        //myTotalValue += myValue
        //Log.i(TAG,"myTotalValue: %,.2f".format(myTotalValue))
        holder?.bind(cryptoList?.get(position)!!, position, listener)
    }


    private fun getPercentColor(percentChang1h: Double?): Int? {
        if (percentChang1h!! < 0) {
            return context?.resources?.getColor(R.color.drop)
        } else if (percentChang1h!! > 0) {
            return context?.resources?.getColor(R.color.rise)
        } else {
            return context?.resources?.getColor(R.color.neutral)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.main_item, parent, false)
        return ViewHolder(v);
    }

    override fun getItemCount(): Int {
        return cryptoList?.size!!
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName = itemView.findViewById<TextView>(R.id.tvName)
        val tvSymbol = itemView.findViewById<TextView>(R.id.tvSymbol)
        val tvPrice = itemView.findViewById<TextView>(R.id.tvPrice)
        val tvPercent1h = itemView.findViewById<TextView>(R.id.tvPercent1h)
        val tvMyQuantity = itemView.findViewById<TextView>(R.id.tvMyQuantity)
        val tvMyValue = itemView.findViewById<TextView>(R.id.tvMyValue)
        val ivICoin = itemView.findViewById<ImageView>(R.id.coin_image)
        val ivOverflow = itemView.findViewById<ImageView>(R.id.ivOverflow)

        fun bind(crypto: Crypto, pos: Int, listener: (Int) -> Unit) = with(itemView) {

            ivOverflow?.setOnClickListener {
                selectedCrypto = crypto
                listener(pos)
                val popup = PopupMenu(context, ivOverflow)

                popup.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener, PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.edit -> {
                                //Log.i(TAG, "edit clicked: ${selectedCrypto?.id.toString()}")
                                showEditDialog()
                                return true
                            }
                            R.id.remove -> {
                                Log.i(TAG, "remove clicked: ${selectedCrypto?.id.toString()}")
                                HomeFragment.removeCrypto(selectedCrypto!!.id!!)
                                return true
                            }
                            else -> {
                                return false
                            }
                        }
                    }

                    private fun showEditDialog() {
                        val dialogBuilder = AlertDialog.Builder(context)
                        val inflater = context.layoutInflater
                        val dialogView: View = inflater.inflate(R.layout.custom_dialog, null)
                        dialogBuilder.setView(dialogView)

                        var cryptoSpinner: SearchableSpinner = dialogView.findViewById(R.id.autocomplete)
                        cryptoSpinner.visibility = View.GONE

                        var etQuantity: EditText = dialogView.findViewById(R.id.crypto_quantity)
                        etQuantity.setText(selectedCrypto?.myQuantity.toString())

                        dialogBuilder.setTitle("Edit Crypto")
                        dialogBuilder.setPositiveButton("Save", { dialog, whichButton ->

                            var quantity = etQuantity.text.toString().trim()
                            if (quantity.isEmpty()) {
                                Toast.makeText(context, "Fill the quantity", Toast.LENGTH_SHORT).show()
                            } else {
                                HomeFragment.editCryptoQuantity(selectedCrypto!!.id!!, quantity.toDouble())
                            }

                        })
                        dialogBuilder.setNegativeButton("Cancel", { dialog, whichButton ->

                        })
                        val b = dialogBuilder.create()
                        b.show()
                    }
                })

                popup.menuInflater.inflate(R.menu.pop_menu, popup.menu)

                popup.show()
            }
        }

    }

}