package ke.co.svs.mykrypto.utils

import com.github.mikephil.charting.charts.BarLineChartBase
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.lang.Long
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Wamae(wamaebenson06@gmail.com) on 27-May-18.
 */

class DayAxisValueFormatter(private val chart: BarLineChartBase<*>) : IAxisValueFormatter {
    override fun getFormattedValue(value: Float, axis: AxisBase?): String {

        val str = value.toString()
        val sf = SimpleDateFormat("H:m")
        val date = Date(Long.parseLong(str))
        System.out.println(sf.format(date))

        return value.toString();
    }


}