package ke.co.svs.mykrypto.utils

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by Wamae(wamaebenson06@gmail.com) on 26-May-18.
 */
class PrefUtil{
    companion object {
        private const val CURRENCY_NAME= "currencyName"
        private const val CURRENCY_VALUE= "currency_value"
        private const val MY_CRYPTOS= "my_cryptos"

        fun getMyCryptos(context: Context?): String {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(MY_CRYPTOS,"")
        }

        fun getCurrencyName(context: Context?): String {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(CURRENCY_NAME,"USD")
        }

        fun getCurrencyValue(context: Context?): Float {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getFloat(CURRENCY_VALUE, 1F)
        }

        fun setMyCrypto(context: Context?, cryptoJSON: String){
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = preferences.edit()
            editor.putString(MY_CRYPTOS, cryptoJSON)
            editor.commit()
        }

        fun setCurrencyName(context: Context, currencyName: String?){
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = preferences.edit()
            editor.putString(CURRENCY_NAME, currencyName)
            editor.commit()
        }

        fun setCurrencyValue(context: Context, currencyValue: Float){
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = preferences.edit()
            editor.putFloat(CURRENCY_VALUE, currencyValue)
            editor.commit()
        }
    }
}