import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.AbsListView
import android.widget.LinearLayout
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import ke.co.svs.mykrypto.utils.PercentFormatter
import ke.co.svs.mykrypto.utils.PrefUtil
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast
import android.view.MenuInflater
import ke.co.svs.mykrypto.Crypto
import ke.co.svs.mykrypto.R


class NetworthFragment : Fragment() {

    companion object {
        val TAG: String = NetworthFragment::class.java.simpleName
        fun newInstance() = NetworthFragment()
    }

    var mToolbar: Toolbar? = null
    var mContext: Context? = null
    var mainLayout: LinearLayout? = null
    var mChart: PieChart? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.networth_fragment, container, false)

        mToolbar = view?.findViewById(R.id.custom_toolbar)

        if (mToolbar != null) {
            (activity as AppCompatActivity).setSupportActionBar(mToolbar)
            mToolbar?.title = "Networth"
        }

        mContext = activity.applicationContext

        mainLayout = view?.findViewById(R.id.main_layout)

        mChart = PieChart(mContext)

        //mChart?.legend?.position = Legend.LegendPosition.ABOVE_CHART_LEFT
        //mChart?.legend?.isEnabled = true
        mChart?.setUsePercentValues(true);
        mChart?.setEntryLabelColor(Color.BLACK)
        mChart?.description?.setEnabled(false)
        mChart?.setExtraOffsets(5F, 10F, 5F, 5F)

        mChart?.dragDecelerationFrictionCoef = 0.95f

        //mChart.setCenterTextTypeface(mTfLight)
        mChart?.centerText = "My Portfolio"

        mChart?.isDrawHoleEnabled = true
        mChart?.setHoleColor(Color.WHITE)

        mChart?.setTransparentCircleColor(Color.WHITE)
        mChart?.setTransparentCircleAlpha(110)

        mChart?.holeRadius = 58f
        mChart?.setTransparentCircleRadius(61f)

        mChart?.setDrawCenterText(true)

        mChart?.setRotationAngle(0F)
        // enable rotation of the chart by touch
        mChart?.isRotationEnabled = true
        mChart?.isHighlightPerTapEnabled = true

        mainLayout?.addView(mChart, AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT))

        initData()

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private fun initData() {
        //TODO: Get BTC data from api


        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);


        val values = ArrayList<PieEntry>()

        val mySavedCrypto = PrefUtil.getMyCryptos(mContext)

        val gson: Gson = GsonBuilder().create();

        var myCryptosList = ArrayList<Crypto>()

        if (!mySavedCrypto.equals("")) {
            myCryptosList = gson.fromJson(PrefUtil.getMyCryptos(mContext), object : TypeToken<List<Crypto>>() {}.type)
        }

        for (crypto in myCryptosList) {
            values.add(PieEntry(crypto.myQuantity?.toFloat()!!, crypto.symbol))
        }


        val dataSet = PieDataSet(values, "")
        dataSet.sliceSpace = 1f
        dataSet.selectionShift = 5f
        dataSet.valueFormatter = PercentFormatter()

        val colors = ArrayList<Int>()

        for (c in ColorTemplate.MATERIAL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        // white space between the slices
        dataSet.setSliceSpace(2f);
        // onClick the slices shift
        dataSet.setSelectionShift(7f);

        val data = PieData(dataSet)
        data.setDrawValues(true)

        mChart?.setData(data)
        mChart?.invalidate()
    }

}
