import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.ListPreference
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import com.google.gson.JsonObject
import com.mohamadamin.kpreferences.base.CompositeDestroyer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ke.co.svs.mykrypto.CryptoAPIService
import ke.co.svs.mykrypto.OnNextScreenCalled
import ke.co.svs.mykrypto.R
import ke.co.svs.mykrypto.utils.PrefUtil


class SettingsFragment : PreferenceFragmentCompat() {

    val cryptoAPIService by lazy {
        CryptoAPIService.create()
    }

    companion object {
        val TAG: String = SettingsFragment::class.java.simpleName
        fun newInstance() = SettingsFragment()
    }

    private var onNextScreenCalledListener: OnNextScreenCalled? = null

    private val destroyers = CompositeDestroyer()

    private var currencyPreference: ListPreference? = null;

    private var entries: Array<CharSequence>? = null
    private var entryValues: Array<CharSequence>? = null
    private var realEntryValues: Array<CharSequence>? = null

    private fun initializePreferences() {

        currencyPreference = preferenceScreen.findPreference("currencyKey") as ListPreference
        setListPreferenceData()

        currencyPreference?.setOnPreferenceClickListener { preference->
            setListPreferenceData()
             false
        }

        currencyPreference?.setOnPreferenceChangeListener { preference, any ->
            var newValue = (any as String).toInt()

            var currencyName: String? = null


            //for((index,value) in entryValues?.withIndex()!!){
                //Log.i(TAG, "setOnPreferenceChangeListener: index: ${index} value: ${value} newValue: ${newValue.toString()} pref: $preference")
               // if(value == newValue.toString()){
                    currencyName = entries?.get(newValue).toString()

               // }
            //}

            Log.i(TAG, "setOnPreferenceChangeListener: currency name: " + currencyName+" currency index: "+newValue+" value: "+realEntryValues?.get(newValue).toString())

            PrefUtil.setCurrencyName(context,currencyName)
            val conversionRate = realEntryValues?.get(newValue).toString().toFloat()
            PrefUtil.setCurrencyValue(context,conversionRate)

            true
        }
    }


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.app_preference)
        initializePreferences()
    }

    protected fun setListPreferenceData() {
        cryptoAPIService.getCurrencies("http://www.apilayer.net/api/live?access_key=8d94201e25ac8bb8276e199aaa6c899b")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> loadCurrencyDetails(result) },
                        { error -> showCurrencyError(error.message) }
                )

    }

    private fun showCurrencyError(message: String?) {
        Log.i(TAG, "showCurrencyError:" + message);
    }

    private fun loadCurrencyDetails(result: JsonObject?) {

        val quotes = result?.getAsJsonObject("quotes")
        val currencyNames = ArrayList<String>()
        val currencyValues = ArrayList<String>()
        val realCurrencyValues = ArrayList<String>()

        for ((index,value)  in quotes?.entrySet()?.withIndex()!!) {
            if(value.key != "USDUSD") {
                currencyNames.add(value.key.replace("USD", ""))
            }else{
                currencyNames.add("USD")
            }
            Log.i(TAG, "loadCurrencyDetails:" + value.value.asString);
            currencyValues.add(index.toString())
            realCurrencyValues.add(value.value.toString())
        }

        entries = currencyNames.toArray(arrayOfNulls<CharSequence>(currencyNames.size))
        entryValues = currencyValues.toArray(arrayOfNulls<CharSequence>(currencyValues.size))
        realEntryValues = realCurrencyValues.toArray(arrayOfNulls<CharSequence>(realCurrencyValues.size));
        currencyPreference?.entries = entries
        currencyPreference?.entryValues = entryValues

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar: Toolbar? = view?.findViewById(R.id.custom_toolbar)

        if (toolbar != null) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            toolbar.title = "Settings"
        }
    }

/*    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.settings, menu)
    }*/

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onNextScreenCalledListener = if (context is OnNextScreenCalled) context else null
    }

    override fun onDetach() {
        super.onDetach()
        onNextScreenCalledListener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        destroyers.invoke()
    }

}