package ke.co.svs.mykrypto.utils

import android.content.Context
import android.util.Log

/**
 * Created by Wamae(wamaebenson06@gmail.com) on 26-May-18.
 */
class CurrencyUtils{
    companion object {
        /**
         * Calculate total value in users currency preference
         */
        fun convertToUserCurrency(context: Context?, value: Double?): Double? {
            val currencyName: String? = PrefUtil.getCurrencyName(context)
            if(currencyName != "USD"){
                Log.i("CurrencyUtils:", "convertToUserCurrency: ${value} ${PrefUtil.getCurrencyValue(context)}")
                return value?.times(PrefUtil.getCurrencyValue(context))
            }
            return value
        }
    }
}