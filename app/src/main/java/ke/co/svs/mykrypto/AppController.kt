package ke.co.svs.mykrypto

import android.app.Application
import android.widget.TextView
import android.support.design.widget.Snackbar
import android.content.ContentValues.TAG
import android.graphics.Color
import android.support.multidex.MultiDex
import android.util.Log
import android.view.View
import com.facebook.stetho.Stetho
import com.mohamadamin.kpreferences.base.KPreferenceManager


/**
 * Created by Wamae on 01-Jan-18.
 */
class AppController : Application() {

    companion object {
        var mInstance: AppController? = null
        @Synchronized
        fun getInstance(): AppController? {
            return mInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this;
        KPreferenceManager.initialize(this)
        Stetho.initializeWithDefaults(this)
        //MultiDex.install(this);
    }

    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener) {
        ConnectivityReceiver.connectivityReceiverListener = listener
    }

    fun showSnackBar(isConnected: Boolean, view: View) {
        var msg = ""
        if (isConnected) {
            Log.e(TAG, "showSnackBar: connected with internet ")
        } else {
            msg = "Something went wrong, Please check your Internet Connetion!"
            Log.e(TAG, "showSnackBar: no internet connection")
        }
        val snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
        /*snackbar.setAction("OK", object : View.OnClickListener() {
            override fun onClick(v: View) {}
        })*/
        snackbar.setActionTextColor(Color.WHITE)
        val view1 = snackbar.view
        val textView: TextView = view1.findViewById(android.support.design.R.id.snackbar_text)
        textView.setTextColor(Color.RED)
        snackbar.show()
    }

}