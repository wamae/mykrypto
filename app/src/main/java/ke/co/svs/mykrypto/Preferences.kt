package ke.co.svs.mykrypto

/**
 * Created by Wamae on 01-Jan-18.
 */
object Preferences {
    val showNotification = "myCryptoNotification" to 0
    val switch = "switchPreference" to true
    val currency = "currencyPreference" to "$"
    val float = "doublePreference" to 3.14f
    val dummy = "dummyPreference" to DummyClass(true, "Default Dummy")
    var myCrypto = "cryptoPreference" to "[]"
}