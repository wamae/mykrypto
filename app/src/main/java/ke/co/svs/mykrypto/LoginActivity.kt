package ke.co.svs.mykrypto

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*


/**
 * Created by Wamae(wamaebenson06@gmail.com) on 27-May-18.
 */
class LoginActivity : AppCompatActivity() {

    val TAG: String = SignupActivity::class.java.simpleName
    private var auth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()

        if (auth?.currentUser != null) {
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            //finish()
        }

        //setSupportActionBar(toolbar);
        btnSignup.setOnClickListener({
            startActivity(Intent(this@LoginActivity, SignupActivity::class.java))
        })

        btnResetPassword.setOnClickListener({
            startActivity(Intent(this@LoginActivity, ResetPasswordActivity::class.java))
        })

        btnLogin.setOnClickListener({
            val email = etEmail?.text.toString().trim()
            val password = etPassword?.text.toString().trim()

            if (TextUtils.isEmpty(email)) {
                Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            progressBar.setVisibility(View.VISIBLE)

            auth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    // there was an error
                    if (password.length < 6) {
                        etPassword.error = getString(R.string.minimum_password)
                    } else {
                        Toast.makeText(this@LoginActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show()
                    }
                } else {
                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

        })

    }
}