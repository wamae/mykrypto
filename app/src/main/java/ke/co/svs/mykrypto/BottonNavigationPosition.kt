package ke.co.svs.mykrypto

import android.support.v4.app.Fragment

/**
 * Created by Wamae on 01-Jan-18.
 */

enum class BottomNavigationPosition(val position: Int, val id: Int) {
    HOME(0, R.id.home),
    NETWORTH(1, R.id.networth),
    ALERTS(2, R.id.alerts),
    SETTINGS(3, R.id.settings);
}

fun findNavigationPositionById(id: Int): BottomNavigationPosition = when (id) {
    BottomNavigationPosition.HOME.id -> BottomNavigationPosition.HOME
    BottomNavigationPosition.NETWORTH.id -> BottomNavigationPosition.NETWORTH
    BottomNavigationPosition.ALERTS.id -> BottomNavigationPosition.ALERTS
    BottomNavigationPosition.SETTINGS.id -> BottomNavigationPosition.SETTINGS
    else -> BottomNavigationPosition.HOME
}

fun BottomNavigationPosition.createFragment(): Fragment = when (this) {
    BottomNavigationPosition.HOME -> HomeFragment.newInstance()
    BottomNavigationPosition.NETWORTH -> NetworthFragment.newInstance()
    BottomNavigationPosition.ALERTS -> AlertsFragment.newInstance()
    BottomNavigationPosition.SETTINGS -> SettingsFragment.newInstance()
}

fun BottomNavigationPosition.getTag(): String = when (this) {
    BottomNavigationPosition.HOME -> HomeFragment.TAG
    BottomNavigationPosition.NETWORTH -> NetworthFragment.TAG
    BottomNavigationPosition.ALERTS -> AlertsFragment.TAG
    BottomNavigationPosition.SETTINGS -> SettingsFragment.TAG
}

