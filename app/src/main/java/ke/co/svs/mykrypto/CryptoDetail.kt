package ke.co.svs.mykrypto

import com.google.gson.annotations.SerializedName

/**
 * Created by Wamae on 30-Dec-17.
 */
data class CryptoDetail(
        @SerializedName("Id") val id: String,
        @SerializedName("Url") val url: String?,
        @SerializedName("ImageUrl") val imageUrl: String?,
        @SerializedName("Name") val name: String?,
        @SerializedName("Symbol") val symbol: String?,
        @SerializedName("CoinName") val coinName: String?,
        @SerializedName("FullName") val fullName: String?,
        @SerializedName("Algorithm") val algorithm: String?,
        @SerializedName("ProofType") val proofType: String?,
        @SerializedName("FullyPremined") val fullyPremined: String?,
        @SerializedName("TotalCoinSupply") val totalCoinSupply: String?,
        @SerializedName("PreMinedValue") val preMinedValue: String?,
        @SerializedName("TotalCoinsFreeFloat") val totalCoinsFreeFloat: String?,
        @SerializedName("SortOrder") val sortOrder: String?,
        @SerializedName("Sponsored") val sponsored: Boolean?)