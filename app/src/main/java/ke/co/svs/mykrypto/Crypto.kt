package ke.co.svs.mykrypto

import com.google.gson.annotations.SerializedName

/**
 * Created by Wamae on 25-Dec-17.
 */
data class Crypto(
        @SerializedName("id") val id: String?,
        @SerializedName("quantity") var myQuantity: Double? = 0.0,
        @Transient var imageUrl: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("symbol") val symbol: String?,
        @SerializedName("rank") val rank: Int?,
        @SerializedName("price_usd") val priceUSD: Double?,
        @SerializedName("price_btc") val priceBTC: Double?,
        @SerializedName("24h_volume_usd") val volumeUSD_24h: Double?,
        @SerializedName("market_cap_usd") val marketCapUSD: Double?,
        @SerializedName("available_supply") val availableSupply: Double?,
        @SerializedName("total_supply") val totalSupply: Double?,
        @SerializedName("max_supply") val maxSupply: Double?,
        @SerializedName("percent_change_1h") val percentChange_1h: Double?,
        @SerializedName("percent_change_24h") val percentChange_24h: Double?,
        @SerializedName("percent_change_7d") val percentChange_7d: Double?,
        @SerializedName("last_updated") val lastUpdated: Int?){
    override fun toString(): String {
        return this.name+" (${this.symbol})"
    }

    override fun equals(other: Any?): Boolean {
        other as Crypto
        if(this.id!!.equals(other.id)){
            return true
        }
        return false
    }
}