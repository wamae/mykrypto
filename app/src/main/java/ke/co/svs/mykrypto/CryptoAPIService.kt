package ke.co.svs.mykrypto

import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.Url


/**
 * Created by Wamae on 25-Dec-17.
 */
interface CryptoAPIService {

    @GET("ticker")
    fun getMyCryptos(@Query("limit") limit: Int):
            Observable<MutableList <Crypto>>

    @GET
    fun getCryptoDetails(@Url url: String): Observable<JsonObject>

    @GET
    fun getCurrencies(@Url url: String): Observable<JsonObject>

    companion object {
        fun create(): CryptoAPIService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .client(client)
                   .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://api.coinmarketcap.com/v1/")
                    .build()

            return retrofit.create(CryptoAPIService::class.java)
        }
    }
}
