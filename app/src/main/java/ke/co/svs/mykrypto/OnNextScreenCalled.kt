package ke.co.svs.mykrypto

/**
 * Created by Wamae on 01-Jan-18.
 */
interface OnNextScreenCalled {
    fun onNextScreenCalled()
}